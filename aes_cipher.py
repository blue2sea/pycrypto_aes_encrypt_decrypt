from Crypto.Cipher import AES
import serial
import time


def read_from_arduino():
    port = "COM4"
    ard = serial.Serial(port, 9600, timeout=5)
    return ard.readline()


if __name__ == '__main__':
    key = 'aaaabbbbccccdddd'
    iv = '1234567812345678'
    while True:
        new_cipher = AES.new(key, AES.MODE_CBC, iv)
        ard_enc = read_from_arduino()[:16]
        origin_text = new_cipher.decrypt(ard_enc).strip()
        print origin_text
        time.sleep(1)
